# Send times from your commit-messages to an API

A small addition to [CaptainHook](https://packagist.org/captainhook/captainhook) that 
will send timing-entries from your commit-messages to an API.

That way you can track your times via your commit-messages.

To use this tool you need to add an entry like the following to your commit-message:

```
Time: 2h15m
```

This will send an entry containing 2 hours and 15 minutes to your timetracking-API of 
choice.

To make sure that all your commit-messages have a time-entry we recommend using the 
[addTime](https://packagist.com/org_heigl/githook_addtime) addOn for CaptainHook.

[![GitLab-CI Bild-Status](https://gitlab.com/heiglandreas/githook_sendtime/badges/rainy-picknick/build.svg)](https://gitlab.com/heiglandreas/githook_sendtime)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/heiglandreas/githook_sendtime/badges/quality-score.png?b=rainy-picknick)](https://scrutinizer-ci.com/g/heiglandreas/githook_sendtime/?branch=rainy-picknick)



## Installation

Install this package using composer

```bash
composer require --dev org_heigl/captainhook_sendtime
```

## Usage

To send the time-entry to your API you'll need to configure the hook using the following 
information:

```json
{
  "commit-msg": {
    "enabled": true,
    "actions": [{
      "action": "\\Org_Heigl\\CaptainHook\\Hooks\\Sendime\\SendTimeAction",
      "options": {
        "account" : "account",
        "backendfactory" : "\\Org_Heigl\\CaptainHook\\Hooks\\SendTime\\Backend\\FileFactory",
        "_comment" : "Following are parameters that are required by the Backend Factory. For Details see the Backend-Documentation",
        "file" : "/tmp/logfile"
      }
    }]
  }
}
```

This will write the times in a text-file residing in `/tmp/logfile`. If you want to use a different backend you will 
need to add the appropriate files via composer and then configure that backend according to the backends documentation.

Currently there are the following backends supported:

* File
* [Tine2.0](https//packagist.org/org_heigl/captainhook_sendtime_backend_tine2)

If you are missing your backend, feel free to create a factory and a backend that implement the Backend-Interface and 
the BackendFactory-interface appropriately.