<?php
/**
 * Copyright Andrea Heigl <andreas@heigl.org>
 *
 * Licenses under the MIT-license. For details see the included file LICENSE.md
 */

namespace Org_Heigl\CaptainHook\Hooks\SendTimeTest\TimeParser;

use DateInterval;
use Org_Heigl\CaptainHook\Hooks\SendTime\TimeParser;
use PHPUnit\Framework\TestCase;
use SebastianFeldmann\Git\CommitMessage;
use UnexpectedValueException;

class TimeParserTest extends TestCase
{

    /** @dataProvider getDurationFromCommitMessageProvider */
    public function testGetDurationFromCommitMessage(string $message, DateInterval $expected)
    {
        $parser = new TimeParser();

        $duration = $parser->getDurationFromCommitMessage(new CommitMessage($message));

        self::assertEquals($expected, $duration);
    }

    /**
     * @dataProvider getFailingDurationFromCommitMessageProvider
     * @expectedException UnexpectedValueException
     */
    public function testGetFailingDurationFromCommitMessage(string $message)
    {
        $parser = new TimeParser();

        $parser->getDurationFromCommitMessage(new CommitMessage($message));
    }

    public function getDurationFromCommitMessageProvider()
    {
        return [
            ["Time\n\nTime: 12h15m", new DateInterval('PT12H15M')],
            ["Time\n\nTime: 12h", new DateInterval('PT12H')],
            ["Time\n\nTime: 15m", new DateInterval('PT15M')],
        ];
    }

    public function getFailingDurationFromCommitMessageProvider()
    {
        return [
            ["Time\n\nTime: 15s"],
            ["Time\n\nTine: 15h12m"],
        ];
    }
}
