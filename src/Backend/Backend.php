<?php
/**
 * Copyright Andrea Heigl <andreas@heigl.org>
 *
 * Licenses under the MIT-license. For details see the included file LICENSE.md
 */

namespace Org_Heigl\CaptainHook\Hooks\SendTime\Backend;


use Org_Heigl\CaptainHook\Hooks\SendTime\Timesheet;

interface Backend
{
    public function sendTimesheet(Timesheet $timesheet) : void;
}