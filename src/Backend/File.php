<?php

declare(strict_types=1);

/**
 * Copyright Andrea Heigl <andreas@heigl.org>
 *
 * Licenses under the MIT-license. For details see the included file LICENSE.md
 */

namespace Org_Heigl\CaptainHook\Hooks\SendTime\Backend;


use function fwrite;
use Org_Heigl\CaptainHook\Hooks\SendTime\Timesheet;
use SplFileInfo;

class File implements Backend
{
    private $filehandle;

    public function __construct(SplFileInfo $file)
    {
        $this->filehandle = fopen($file->getPathname(), 'a');
    }

    public function sendTimesheet(Timesheet $timesheet): void
    {
        fwrite($this->filehandle, sprintf(
            '"%s","%s","%s","%s"' . "\n",
            $timesheet->getDate()->format('Y-m-d H:i:s'),
            $timesheet->getDuration()->format('%H:%I'),
            $timesheet->getAccount(),
            $timesheet->getDescription()
        ));
    }

    public function __destruct()
    {
        fclose($this->filehandle);
    }
}