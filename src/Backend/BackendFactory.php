<?php

declare(strict_types=1);

/**
 * Copyright Andrea Heigl <andreas@heigl.org>
 *
 * Licenses under the MIT-license. For details see the included file LICENSE.md
 */

namespace Org_Heigl\CaptainHook\Hooks\SendTime\Backend;


use CaptainHook\App\Config\Options;

interface BackendFactory
{
    public function createBackend(Options $options) : Backend;
}