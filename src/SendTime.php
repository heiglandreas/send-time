<?php

declare(strict_types=1);

/**
 * Copyright Andrea Heigl <andreas@heigl.org>
 *
 * Licenses under the MIT-license. For details see the included file LICENSE.md
 */

namespace Org_Heigl\CaptainHook\Hooks\SendTime;


use CaptainHook\App\Config;
use CaptainHook\App\Console\IO;
use CaptainHook\App\Hook\Action;
use DateTimeImmutable;
use Exception;
use Org_Heigl\CaptainHook\Hooks\SendTime\Backend\Backend;
use SebastianFeldmann\Git\CommitMessage;
use SebastianFeldmann\Git\Repository;

class SendTime implements Action
{
    private $backend;

    private $parser;

    public function __construct(Backend $backend, TimeParser $parser)
    {
        $this->backend = $backend;
        $this->parser  = $parser;
    }

    /**
     * Executes the action.
     *
     * @param  \CaptainHook\App\Config $config
     * @param  \CaptainHook\App\Console\IO $io
     * @param  \SebastianFeldmann\Git\Repository $repository
     * @param  \CaptainHook\App\Config\Action $action
     * @throws \Exception
     */
    public function execute(Config $config, IO $io, Repository $repository, Config\Action $action) : void
    {
        $options = $action->getOptions();

        try {
            $duration = $this->parser->getDurationFromCommitMessage($this->getCommitMessage());

            $timesheet = new Timesheet(
                $this->getCommitDate(),
                $this->getDescription(),
                $options->get('account'),
                $duration
            );

            $this->backend->sendTimesheet($timesheet);
        } catch (Exception $e) {
            $io->writeError($e->getMessage());
        }
    }

    private function getDescription() : string
    {
        exec('git log -1 --format="%h"', $response);
        $hash = $response[0];
        $response = [];
        exec('git log -1 --format="%s"', $response);
        $subject = $response[0];

        return $subject . ' (' . $hash . ')';

    }

    private function getCommitDate() : DateTimeImmutable
    {
        exec('git log -1 --format="%cD"', $response);

        return new DateTimeImmutable($response[0]);
    }

    private function getCommitMessage() : CommitMessage
    {
        exec('git log -1 --format="%B"', $result);

        return new CommitMessage(implode("\n", $result));
    }
}