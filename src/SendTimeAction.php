<?php

declare(strict_types=1);

/**
 * Copyright Andrea Heigl <andreas@heigl.org>
 *
 * Licenses under the MIT-license. For details see the included file LICENSE.md
 */

namespace Org_Heigl\CaptainHook\Hooks\SendTime;


use CaptainHook\App\Config;
use CaptainHook\App\Console\IO;
use CaptainHook\App\Hook\Action;
use Org_Heigl\CaptainHook\Hooks\SendTime\Backend\BackendFactory;
use SebastianFeldmann\Git\Repository;
use UnexpectedValueException;

class SendTimeAction implements Action
{

    /**
     * Executes the action.
     *
     * @param  \CaptainHook\App\Config $config
     * @param  \CaptainHook\App\Console\IO $io
     * @param  \SebastianFeldmann\Git\Repository $repository
     * @param  \CaptainHook\App\Config\Action $action
     * @throws \Exception
     */
    public function execute(Config $config, IO $io, Repository $repository, Config\Action $action) : void
    {
        $options = $action->getOptions();

        $backendFactory = $options->get('backendfactory');
        $backendFactory = new $backendFactory();
        if (! $backendFactory instanceof BackendFactory) {
            throw new UnexpectedValueException(sprintf(
                'Expected an instance of BackendFactory but got %s',
                get_class($backendFactory)
            ));
        }

        $myaction = new SendTime(
            $backendFactory->createBackend($options),
            new TimeParser()
        );

        $myaction->execute($config, $io, $repository, $action);
    }
}