<?php

declare(strict_types=1);

/**
 * Copyright Andrea Heigl <andreas@heigl.org>
 *
 * Licenses under the MIT-license. For details see the included file LICENSE.md
 */

namespace Org_Heigl\CaptainHook\Hooks\SendTime;

use DateInterval;
use function preg_match;
use SebastianFeldmann\Git\CommitMessage;
use UnexpectedValueException;

class TimeParser
{
    private $regex = '/^\s*Time: (((?P<hour>\d+)h((?P<hourminute>\d+)m)?)|((?P<minute>\d+)m))\s*$/';

    public function getDurationFromCommitMessage(CommitMessage $message) : DateInterval
    {
        foreach ($message->getLines() as $line) {
            if (! preg_match($this->regex, $line, $result)) {
                continue;
            }
            if (isset($result['minute'])) {
                return new DateInterval('PT' . $result['minute'] . 'M');
            }

            $interval = 'PT';
            if (isset($result['hour'])) {
                $interval = $interval . $result['hour'] . 'H';
            }
            if (isset($result['hourminute'])) {
                $interval = $interval . $result['hourminute'] . 'M';
            }

            return new DateInterval($interval);
        }

        throw new UnexpectedValueException('No Time found in the message');
    }
}