<?php

declare(strict_types=1);

/**
 * Copyright Andrea Heigl <andreas@heigl.org>
 *
 * Licenses under the MIT-license. For details see the included file LICENSE.md
 */

namespace Org_Heigl\CaptainHook\Hooks\SendTime;


use DateInterval;
use DateTimeImmutable;

class Timesheet
{
    private $date;

    private $description;

    private $account;

    private $duration;

    public function __construct(DateTimeImmutable $date, string $description, string $account, DateInterval $duration)
    {
        $this->date = $date;
        $this->description = $description;
        $this->account = $account;
        $this->duration = $duration;
    }

    public function getDate() : DateTimeImmutable
    {
        return $this->date;
    }

    public function getDescription() : string
    {
        return $this->description;
    }

    public function getAccount(): string
    {
        return $this->account;
    }

    public function getDuration() : DateInterval
    {
        return $this->duration;
    }
}